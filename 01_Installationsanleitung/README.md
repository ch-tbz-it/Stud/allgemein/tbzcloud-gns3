# Installationsanleitung - TBZ Cloud - GNS3

<big><b><span style="color: red; background-color: yellow;">Achtung: Dies ist nicht die Installationsanleitung für Lernende, die auf ihr GNS3 Labor zugreifen möchten!</span></b></big>

<big>Siehe [02_Bedienungsanleitungen](../02_Bedienungsanleitung/)</big>

**Zielpublikum: Personen, die eine eigene private Cloud für GNS3 aufsetzen möchten oder Lehrpersonen, welche die Nodes steuern bzw. neuaufsetzen möchten**


## Preclaimer
 1. Diese Installationsanleitung dient zur lediglich zur Orientierung und ist nicht abschliessend. 
 2. Diese Installationsanleitung eignet sich vor allem, wenn die einzelnen Nodes (Physische PC/Servers) eher wenig Leistung (<64GB RAM, CPU Benchmark <20000) haben (vgl. [Infrastruktur](../00_Infrastruktur/)) und GNS3 direkt ohne zusätzliche Virtualisierungsschicht installiert wird. Ein alternatives Konzept ist der Einsatz von ein bis zwei leistungsfähigen Servers (>128 GB RAM, Benchmark >30000), die als KVM Nodes dienen und einem kleinen Regio+Rack Controller. 
 3. Das Deployment der Nodes und die Verwaltung der GNS3 User Sessions übernimmt die eigens dafür entwickelte Software [GNS3aaS](https://gitlab.com/gns3aas) 

# Planung

## Hardwareanforderungen
 - Mindestens zwei physische Nodes<br><i>Pro 20 User ca. 5 Maschinen (Bei Einsatz von GNS3 zum Aufbau einer Windows Domain: ca. 1 User pro Maschine).</i>
   - Intel(R) Core(TM) i7-4790 CPU, 8 cores, 3.6 GHz o.ä.
   - mind. 16 GB RAM
   - mind. 256 GB SSD
   - mind. 1 GBit/s RJ45 (1000BASE-T)
   - Remote out-of-band management mit Start, Reboot und Shutdown Funktion (vgl. [Intel AMT](https://en.wikipedia.org/wiki/Intel_Active_Management_Technology), siehe https://maas.io/docs/power-management-reference)
 - Gigabit Switc,Ports: Anzahl Nodes + Regio+Rack Controller + 2 Wartungsports
 - Patch-Kabel
 - Router mit Port Forwarding Funktion


## Weitere Voraussetzungen
 - Ein designiertes IPv4 Subnetz
 - 1 x Öffentliche IP-Adresse
 - [MAAS installation requirements](https://maas.io/docs/maas-installation-requirements)

## Beispiel IP-Adressverteilung
 - Tipp: Kein Subnetz verwenden, dass häufig verwendet wird!
 - Beispiel IPv4 Subnetz: 10.13.162.0/24
 - Idee Adressierungsschemas: IP Adressen lassen sich aus dem Namen ableiten und umgekehrt.

| Number | Name             | Out-Of-Band Interface | Node Interface   |
|--------|------------------|-----------------------|------------------|
| 1      | Router           | N/A                   | 10.13.162.1/24   |
| 10     | CONTROLLER       | 10.13.162.10/24       | 10.13.162.10/24  |
| 20     | MY-CLOUD-NODE-20 | 10.13.162.20/24       | 10.13.162.120/24 |
| 21     | MY-CLOUD-NODE-21 | 10.13.162.21/24       | 10.13.162.121/24 |
| 22     | MY-CLOUD-NODE-22 | 10.13.162.22/24       | 10.13.162.122/24 |
| 23     | MY-CLOUD-NODE-23 | 10.13.162.23/24       | 10.13.162.123/24 |
| 24     | MY-CLOUD-NODE-24 | 10.13.162.24/24       | 10.13.162.124/24 |
| 25     | MY-CLOUD-NODE-25 | 10.13.162.25/24       | 10.13.162.125/24 |
|        | usw.             |

## Zugangsdaten
Folgende Zugangsdaten werden verwendet und sollten sich unterscheiden:
 - Out-Of-Band Management
 - Ubuntu Rack+Regio Controller WebUser
 - Notfall SSH Key für Rack+Regio Controller

# Installation

## Netzwerk vorbereiten
 - Die Konfiguration von Router und Switch ist in dieser Anleitung nicht enthalten. 
 - Die Verkabelung (Storm, Netzwerk) ist in der Anleitung nicht enthalten.

## Hardware vorbereiten
 - Out-Of-Band Management aktivieren und wenn notwendig Password festlegen
 - Statische IP Adressen im Out-Of-Band Management festlegen **oder** Statische Adresszuteilung im DHCP Server mithilfe der MAC-Adresse zuweisen
 - Der Vorgang ist je nach eingesetzter Hardware unterschiedlich. Das Vorgehen muss dem Manual des entsprechenden Typs entnommen werden. 
 - BIOS Bootpriorität: Netzwerk-Boot in die Position (PXE Boot)
 - Gesamte Hardware verkabeln bzw. Netzwerk verkabeln
 - Alle Nodes ausgeschaltet lassen

## Installation Rack+Regio Controller
 - Prüfen, welche Version mit der gewünschten MAAS Version kompatibel ist. 
 - [Ubuntu Server Image](https://ubuntu.com/download/server) herunterladen und auf einen USB Stick flashen
 - Ubuntu installieren, statische IP-Adresse zuweisen
 - Grundsätzlich erfolgt die Installation gemäss Anleitung auf https://maas.io/docs/how-to-install-maas

Folgende Befehle wurden bei Installation des TBZ Cloud GNS3 Regio+Rack Controllers ausgeführt:
```
$ sudo apt autoremove cloud-init
$ sudo apt install ifupdown
$ sudo apt-add-repository ppa:maas/3.2
$ sudo apt-get update
$ sudo apt-get -y install maas
$ sudo maas createadmin
$ sudo apt install wsmancli amtterm apache2 php libapache2-mod-php -y
```

Anschliessend ist das WebGui via http://INSER-YOUR-IP-HERE:5240/MAAS erreichbar.

## Nodes hinzufügen
 - Alle Nodes starten und einzeln über die *MAAS WebGui* hinzufügen (Commissioning -> Ready)
 - In den einzelnen Nodes jeweils konfigurieren:
   - Power configuration im Tab "Configuration"
   - Statische IP-Adresse im Tab Network dem entsprechenden interface zuweisen.
---
![edit interface](media/editphysical.PNG)
<br>*Screenshot option to edit physical network interface configuration*

---
![assign static ip](media/assignstaticip.PNG)
<br>*Screenshot assign static IP to interface according to own address plan*

---
![assign static ip](media/examplenode.PNG)
<br>*Screenshot example summary view of node from TBZ Cloud*

---

# GNS3aaS

Das Deployment der Maschinen und die Bereitstellung über eine Self-Service-Platform erfolgt mithilfe der Software [GNS3aaS](https://gitlab.com/gns3aas/). 

Die wichtigen Funktionen von GNS3aaS:
 - Vollautomatisches Deployment von GNS3 inkl. Images auf den Nodes mit cloud-init
 - Automatische Verwaltung der VPN Zugänge (OpenVPN und WireGuard)
 - Self-Service Platform für Lernende und Lehrpersonen über Web-GUI

SuperUser und Administratoren können die aktiven Sessions und das Deployment über das [CLI Tool](https://gitlab.com/gns3aas/tbz-manual/-/blob/main/GruppeAutorisieren.md) vornehmen. 


