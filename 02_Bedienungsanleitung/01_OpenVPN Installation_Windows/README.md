## 1.1. OpenVPN

Die Verbindung zum GNS3 Server erfolgt über ein Layer 2 OpenVPN Tunnel.

(Empfehlung für macOS User: [WireGuard](https://www.wireguard.com/))

Führen Sie diese Schritte aus, sofern Sie den OpenVPN Community Client noch nicht installiert haben.

Das Video ist ergänzend zur Anleitung und nebst der Installation auch noch einen ping-Test. 

![OpenVPN Installation Video](videos/OpenVPN_Installation.webm)

| N°| Schritt | Screenshot |
|---|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------|
| 1 | Laden Sie den OpenVPN Community Client auf  <https://openvpn.net/community-downloads/> herunter.                                                                                                                                                                                                                         | ![](media/643edf407ae9a374f0119ffc059bb1f2.png)  |
| 2 | Führen Sie die Installation mit den Standarteinstellungen durch.                                                                                                                                                                                                                                                         |                                                  |
| 3 | Die OpenVPN Zugangsdaten können direkt auf https://cloud.edu.tbz.ch/gns3 bezogen werden. Dafür muss zuerst eine Session gestartet werden. **Wichtig: Die OpenVPN-Keys ändern bei jedem Session-Neustart. Die WireGuard-Keys bleiben, bis die Session gelöscht wird, gleich.**                                                                                          |                                                  |
| 4 | Erstellen Sie im Pfad  C:\\Users\\*YOUR USERNAME*\\OpenVPN\\config  einen Ordner «gns3lab» und legen Sie die *ovpn* Datei dort ab.  (Der Dateiname ist je nach Server leicht unterschiedlich.)  Hinweis: Teilweise funktioniert es mit dem Unterordner nicht. In diesem Fall die ovpn Datei direkt im *config* Ordner ablegen.                                                                                            | ![](media/d251687d8620426c0e13e53aa805e898.png)  |
| 5 | In der Taskleiste finden Sie das OpenVPN  Symbol (![](media/bff87c4dbcc1aac6a8a55b4627fc6800.png)). Klicken Sie mit der rechten Maustaste auf das Symbol und wählen Sie «Verbinden».  *Hinweis: Wenn Sie bereits OpenVPN auf Ihrem Computer installiert haben, hat es möglicherweise mehrere Verbindungen zur Auswahl.*  | ![](media/2f5c656186c63946ced240b44ba5a85a.png)  |
| 6 | Ein Statusfenster mit einem Log öffnet sich. Sobald Sie verbunden sind, schliesst sich das Fenster.                                                                                                                                                                                                                      | ![](media/cb697e4930ae6a44e90b9af14aa0684e.png)  |
| 7 | Haben Sie sich erfolgreich verbunden, so wird das Symbol grün.                                                                                                                                                                                                                                                           | ![](media/f505e6f0f0ab4cc5b0578135b88e4dfe.png)  |
