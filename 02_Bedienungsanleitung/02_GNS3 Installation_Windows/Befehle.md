# MikroTik Befehle

### Name setzen
```
/system/identity/ set name=[name]
```
Beispiel: Name soll *R1* lauten:
```
/system/identity/ set name=R1
```

### IPv4 Adresse hinzufügen
```
/ip/address/ add address=[ip/mask] interface=[interface]
```
Beispiel interface *ether3* soll die IP-Adresse *192.168.255.1/30* erhalten:
```
/ip/address/ add address=192.168.255.1/30 interface=ether3
```

### IPv4 Route hinzufügen
```
/ip/address/ add address=[ip/mask] interface=[interface]
```
Beispiel interface *ether3* soll die IP-Adresse *192.168.255.1/30* erhalten:
```
/ip/address/ add address=192.168.255.1/30 interface=ether3
```

### Gesamte Konfiguration anzeigen
```
export
```