# TBZ Cloud - GNS3 - Installation <!-- omit in toc -->

![](media/1d36f9957c84bab58e0427f5e16c075d.png)


## 1.2. GNS3 Client

Führen Sie die nachfolgenden Schritte aus, um auf Ihrem Computer den GNS3 Client zu installieren.

**Wichtig: Die GNS3 Version des Clients muss mit der Serverversion übereinstimmen! Informieren Sie sich beim Kursleiter über die aktuelle Serverversion!**

![OpenVPN Installation Video](videos/GNS3_Installation.webm)

| N°| Schritt | Screenshot |
|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| 1 | Laden Sie auf *github* den GNS3 Installer herunter.  <https://github.com/GNS3/gns3-gui/releases>  **Wichtig: Version muss mit Version auf Server übereinstimmen.**                               | ![](media/d7b2b1e2e87020f8ce82e54e01bcdc6b.png)                                                                           |
| 2 | Starten Sie den Installer und wählen Sie «Next».  Lesen die die Lizenzbestimmungen durch und klicken Sie danach «I Agree».  Im «Start Menu Folder»-Dialog wählen Sie «Next». | ![](media/ba7b378e70751c0ef2d4dcb9b0dea899.png) *Ignorieren Sie Versionsnummer in diesem und nachfolgenden Screenshots.*  |
| 3 | Wählen Sie die Komponenten gemäss Screenshot aus.   *Wenn Sie auf dem Server arbeiten, benötigen Sie keine lokale VM.*                                                     | ![](media/2b71fbb7c4980344169ad43165e69695.png)![](media/5eea925e2cd5adad3736d61773b4d075.png)                            |
| 4 | Akzeptieren Sie die Standardeinstellungen und wählen Sie «Next».                                                                                                             | ![](media/fbd94b65f10c791b49e7bb818ebe40e3.png)                                                                           |
| 5 | Warten Sie, bis die Installation abgeschlossen ist.                                                                                                                          | ![](media/e616e42e2e25177cdc22b15944d49bb9.png)                                                                           |
| 6 | Wählen Sie «No» und klicken Sie auf «Next».                                                                                                                                  | ![](media/713478c99be498bfda4728299fe323a7.png)                                                                           |
| 7 | Wählen Sie «Start GNS3» ab (uncheck) und schliessen Sie die Installation mit «Finish» ab.                                                                                    | ![](media/4f2b307ca6134a5c1c795d734a591b00.png)                                                                           |


## 1.3. Verbindungstest

Stellen Sie mit einem **ping** sich sicher, dass Sie den Server erreichen können:

![](media/467b167e3b28df1a3bda6b9d6e07a24a.png)

## 1.4. GNS3 GUI

Bevor Sie die GNS3 GUI starten, stellen Sie sicher, dass Sie per OpenVPN mit dem Server verbunden sind.


Das nachfolgende Video ist ergänzend zur Anleitung und zeigt, wie nach der Installation ein GNS3 gestartet und ein Projekt gestartet werden kann. 

| 1 | Starten Sie GNS3.                                                                              | ![](media/89e2acfd0ba569dd9ad35b24719979c8.png) |
|---|------------------------------------------------------------------------------------------------|-------------------------------------------------|
| 2 | Wählen Sie *Edit* *Preferences* und dann *Server*                                              |                                                 |
| 3 | Füllen Sie die Felder wie im Screenshot aus und klicken Sie anschliessen auf «OK».             | ![](media/39b5634f949316c5248c9218f8131974.png) |
| 4 | Schliessen Sie GNS3 und öffnen Sie es erneut, damit die Einstellungen korrekt geladen werden.  |                                                 |
| 5 | Wenn alles erfolgreich war, sehen Sie jetzt das «Project»-Fenster.                             | ![](media/a2a3cdcd03eca9a2e4e014fd7836429a.png) |

**Wenn Sie sich erfolgreich mit dem Server verbunden haben, fahren Sie mit der [GNS3 Einführung](../20_GNS3%20Einführung/) fort.**

# 2. Wichtige Hinweise

## 2.1. Mehrere Projekte auf dem gleichen Node

### 2.1.1. IP-Konflikte vermeiden

![Grafik mehrere Hosts](media/multipleprojects.PNG)

Der Zugang auf das GNS3 Labor erfolgt über eine Layer2-VPN Verbindung. Das [TAP-interface](https://de.wikipedia.org/wiki/TUN/TAP) ist mit br0 verbunden. Alle emulierten Geräte innerhalb der Projekte, die direkt oder über einen Switch mit br0 verbunden sind und eine Adresse im **.23** Subnetz haben, befinden sich im selben Layer2 Netz bzw. in der gleichen Broadcast-Domäne. 

Es gilt IP-Adress-Konflikte zu vermeiden. Die IP-Adressierung im **192.168.23.0/24** Subnetz muss über alle Projekte hinweg koordiniert werden. 

### 2.1.2. Unterschiedliche Projektnamen
Jeder Benutzer hat seine Projekte unterschiedlich zu bezeichnen. Zum Beispiel mit dem Anbringen eines persönliches Kürzels.

Beispiel
```
ALP 00 Ping mit zwei VPCS
```

**Wenn Sie ein neues Projekt erstellen, welches denselben Namen hat wie ein Bestehendes, wird dieses überschrieben.**
