# GNS3 - Installation unter macOS

## Teil 1 - GNS3 Herunterladen und mit Server verbinden

Die Installation von GNS3 unter macOS erfolgt auf typische Art und Weise:
 1. DMG von [GitHub - GNS3 - Releases](https://github.com/GNS3/gns3-gui/releases) in der **richtigen Version (siehe TBZ Cloud Session)** herunterladen.
 2. DMG öffnen und `GNS3.app` in den `Applications` Ordner verschieben
 3. DMG unmounten
 4. `GNS3.app` öffnen
 5. In der Initialen Konfiguration die Option `Remote Server` (letzte Option) wählen. 
 
![GNS3 Init 1](./media/GNS3%20Init%201.png)

 7. Host: 192.168.23.1, Authentification: keine

![GNS3 Init 1](./media/GNS3%20Init%202.png)


## Teil 2 - VNC Konfigurieren

Der Standard VNC Viewer von macOS ist nicht mit GNS3 kompatibel. Deshalb muss ein alternativer VNC Viewer installiert werden. 

 1. Auf https://tigervnc.org/ TigerVNC für macOS herunterladen. <br><i>Direktlink zu Sourceforge: https://sourceforge.net/projects/tigervnc/files/stable/1.13.1/</i><br>**Wenn möglich neuere Version herunterladen!**

![TigerVNC Download](./media/TigerVNC%20Download.png)

 2. `TigerVNC Viewer 1.13.1.app` (Version kann abweichen) in `Applications` Folder verschieben

![TigerVNC Application Folder](./media/TigerVNC%20Viewer.png)

 3. In `GNS3.app` Preferences öffnen

![GNS3 Preferences](./media/GNS3%20Preferences.png)

 4. *General* => *VNC* => *Edit*

![GNS3 VNC 1](./media/GNS3%20VNC.png)

 5. Custom wählen und `/Applications/TigerVNC\ Viewer\ 1.13.1.app/Contents/MacOS/TigerVNC\ Viewer %h::%p` als Befehl hinterlegen<br>**Achtung! Wenn TigerVNC Version abweicht, muss der Befehl entsprechend angepasst werden!**

![GNS3 VNC 2](./media/GNS3%20VNC%20Edit.png) 

 6. Konfiguration abspeichern: *OK* => *OK*

