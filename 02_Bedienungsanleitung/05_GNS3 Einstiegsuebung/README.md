# GNS3 Einführungsübung - Zwei VPCs pingen sich gegenseitig

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1/2 Lektion |
| Ziele | Sie sind in der Lage ein GNS3 Projekt mit zwei VPCS zu erstellen, die sich gegenseitig pingen können. |

![GNS3 Screenshot zweigt zwei VPCS](images/2VPCS.png)

## 1.1. Vorgehen  <!-- omit in toc -->
Schauen Sie dieses Video an und bauen Sie das Labor in GNS3 nach. 

![GNS3 Video Zwei VPCS pingen](videos/GNS3_Intro_zwei_vpcs_ping.webm)

## 1.2. Hinweise  <!-- omit in toc -->
 - Das Subnetz 192.168.23.0/24 ist gegeben. 
 - Die IP Adresse 192.168.23.1/24 ist für den GNS3 drei Server reserviert. Diese Adresse dürfen Sie **NIE** verwenden, sonst verliert die GNS3 GUI die Verbindung zum GNS3-Server.
 - Im Subnetz 192.168.23.0/24 ist ein DHCP Server aktiv, der IP-Adressen ab 192.168.23.128 bis 192.168.23.200 vergibt. Diesen IP-Range sollten sie nicht statisch vergeben. 

