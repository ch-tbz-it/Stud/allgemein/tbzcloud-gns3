# GNS3 - Basislabor
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1/2 Lektion |
| Ziele | Sie sind in der Lage das Basislabor zu konfigurieren. |

Dieses Labor kann als Grundlage für andere Laborsetups verwendet werden, welche Zugriffe ins Internet benötigen und bei denen der Zugriff direkt vom eigenen Laptop ins Labor gewünscht ist. 

 - Internet Uplink via NAT
 - Management Netzwerkzugriff via BR0 (Direkter Zugriff vom Laptop in das Netzwerk)

## Vorgehen
|   |   |
|---|---|
| 1. OpenWRT Router in Labor einfügen.  | ![OpenWrt](./images/OpenWRT.png)  |
| 2. Rechte Maustaste => *Configure* | ![OpenWRT Configure](./images/ConfigureOpenWRT.png) |
| 3. Unter *Network* => *Adapter* auf 8 setzen und anschliessend mit *OK* speichern. | ![OpenWRT Configure Adapter](./images/OpenWRT%20Network%20Adapter.png) |
| 4. Rechte Maustaste => *Start* | ![OpenWRT Start](./images/OpenWRT%20Start.png) |
| 5. Restliche Elemente einfügen und miteinander verbinden | ![OpenWRT](./images/OpenWRT%20Lab%20Complete.png) |
| 6. OpenWRT *Console* öffnen | ![OpenWRT Console](./images/OpenWRT%20Console.png) |
| 7. Netzwerkkonfigurationsdatei mit `vim /etc/config/network` öffnen | ![OpenWRT Console Nano](./images/OpenWRT%20Console%20Nano.png) |

**Du kennst VIM nicht? Crash Course Video anschauen auf [YouTube!](https://www.youtube.com/results?search_query=vim+crash+course)**

8. Konfiguration so anpassen, dass sie nachher wie folgt aussieht:
```
config interface 'loopback'
        option device 'lo'
        option proto 'static'
        option ipaddr '127.0.0.1'
        option netmask '255.0.0.0'

config globals 'globals'
        option ula_prefix 'fd96:ed91:0951::/48'

config device
        option name 'br-lan'
        option type 'bridge'
        list ports 'eth2'
        list ports 'eth3'
        list ports 'eth4'
        list ports 'eth5'
        list ports 'eth6'
        list ports 'eth7'

config interface 'lan'
        option device 'br-lan'
        option proto 'static'
        option ipaddr '192.168.10.1'
        option netmask '255.255.255.0'
        option ip6assign '60'

config interface 'wan'
        option device 'eth0'
        option proto 'dhcp'

config interface 'mgmt'
        option device 'eth1'
        option proto 'dhcp'
        option defaultroute '0'
        option peerdns '0'

config interface 'wan6'
        option device 'eth0'
        option proto 'dhcpv6'
```

8. `vim` verlassen und mit `service network reload` Konfiguration anwenden.
9. Mit `vim /etc/config/firewall` Firewall-Konfigurationsdatei öffnen.
10. Zone `defaults` so anpassen, dass eingehende Verbindungen erlaubt werden (`option input ACCEPT`)

**Achtung**: Nachfolgende Konfiguration ist nur ein Ausschnitt und NICHT die gesamte Konfigurationsdatei!

```
config defaults
        option syn_flood        1
        option input            ACCEPT
        option output           ACCEPT
        option forward          REJECT
```

Nach der Änderung mit `service firewall reload` die Firewall Konfiguration neu einlesen. 

11. Zugriff auf Webinterface von OpenWRT. Zuerst IP-Adresse ermitteln

![OpenWRT ifconfig](./images/OpenWRT%20ifconfig.png)

12. Anschliessend IP-Adresse im Webbrowser aufrufen und mit Username `root` password `root` einloggen. 

![OpenWRT ifconfig](./images/OpenWRT%20WebGui.png)

13. `Network` => `Interfaces` bei `MGMT` auf `Edit` klicken.

![OpenWRT Network Interface Step1](./images/OpenWRT%20MGMT%20Interface%20Step1.png)

14.  Im Tab `Firewall Setting` so anpassen, dass es nachher wie auf dem Screenshot aussieht (Tipp: `mgmt` in `custom` eingeben):

![OpenWRT Network Interface Step1](./images/OpenWRT%20MGMT%20Interface%20Step2.png)

15.   Firewall-Regeln anpassen. `Masquerading` setzen wie auf Bild und anschliessend bei `LAN => WAN` auf `Edit` klicken. 

![OpenWRT Firewall](./images/OpenWRT%20Firewall.png)

16.    Interface `mgmt` hinterlegen

![OpenWRT Firewall](./images/OpenWRT%20Firewall%20Add%20Interface.png)

17.   Bei `mgmt` `forward` auf `accept` setzen, sicherstellen, dass bei `wan` => `input` => `reject` steht und Konfiguration anschliessend anwenden.

![OpenWRT Firewall Save and Apply](./images/OpenWRT%20SaveApply%20Firewall.png)

18.   Testen in der Ubuntu VM, ob der Zugriff aufs Internet möglich ist und ob `192.168.23.1` angepingt werden kann. 

![UbuntuVM](./images/UbuntuVM.png)

Beispielprojekt für direkten Import in GNS3 kann [hier](./OpenWRT_Basislabor.gns3project) heruntergeladen werden. 
