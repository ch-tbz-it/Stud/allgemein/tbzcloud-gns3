# TBZ Cloud - GNS3 - Bedienungsanleitungen

## Einleitung

**Zielpublikum**: Endbenutzer (Lernende) einer **TBZ Cloud GNS3 Session**

Diese Anleitungen in diesem Abschnitt ermöglichen dem Leser:
 - OpenVPN einzurichten für den Zugriff in die TBZ Cloud
 - GNS3 auf dem eigenen Gerät zu installieren 
 - GNS3 für den Serverzugriff zu konfigurieren

Die Anleitung richtet sich an Windows 10/11 Benutzer. 

Informationen für macOS Benutzer sind am Ende dieser Seite aufgeführt

## Leistungsfähigkeit TBZ Cloudserver und allgemeine Informationen

Da GNS3 jedes einzelne Netzwerkgerät emuliert, beziehungsweise eine virtuelle Maschine startet, benötigt das viel Leistung. Beachten Sie dabei folgendes:

-   Der Server hat nur begrenzt Leistung.
-   Starten Sie **nicht** alle Netzwerkgeräte auf einmal, um die Festplatte nicht auszulasten.
-   Emulieren Sie **nicht** mehr als 6 bis 7 Netzwerkgeräte pro Projekt.
-   Achten Sie auf die Auslastungsangaben im GNS3.

Natürlich dürfen Sie sich zusätzlich einen eigenen GNS3 Server auf Ihrem Laptop oder einem anderen Server aufsetzen. In der [GNS3 Dokumentation](https://docs.gns3.com/) finden Sie dafür reichlich Anleitungen. Für die Gruppenübungen empfehle ich Ihnen, den zur Verfügung gestellten Server zu verwenden.

---

## Zugriff auf eigene TBZ Cloud GNS3 Session

Mit der TBZ-E-Mail-Adresse kann unter https://cloud.edu.tbz.ch/gns3 eine GNS3 Session in der **TBZ Cloud** gestartet werden. 

Über diese Self-Service Platform muss der VPN-Key heruntergeladen werden, der anschliessend benötigt wird. 

**Wichtig:**
 - Der OpenVPN-Key ändert sich nach jedem Session Start
 - Der WireGuard-Key bleibt so lange gültig, bis die Session am Ende des Quartals gelöscht wird.
 - OpenVPN ist als Layer2-VPN konfiguriert. 
 - WireGuard ist ein Layer3-VPN. Gewisse Übungen sind mit dem WireGuard-VPN nicht ohne Änderung möglich. 

---

## Installation (Windows)

**[01_OpenVPN Installation](01_OpenVPN%20Installation_Windows/)**

Schritt für Schritt Anleitungen für:
 - Installation von OpenVPN auf Windows 10/11

---

**[02_GNS3 Installation](02_GNS3%20Installation_Windows/)**

Schritt für Schritt Anleitung für:
 - Installation von GNS3 auf Windows 10/11
 - GNS3 für den Zugriff auf die TBZ Cloud konfigurieren

---

## Installation (macOS)

**[03_GNS3 Installation MacOS](./03_GNS3%20Installation%20MacOS/)**

Schritt für Schritt Anleitung für:
 - Installation von GNS3 unter macOS
 - GNS3 für den Zugriff auf die TBZ Cloud konfigurieren
 - TigerVNC installieren

**Hinweis: Unter macOS ist kann OpenVPN nicht verwendet werden. Eine Anleitung für die Verwendung mit WireGuard ist in der TBZ Cloud verlinkt**

---

## Einstiegsübungen

Die nachfolgenden Übungen dienen als Einstieg in GNS3. 

 - **[Zwei VPCs pingen sich gegenseitig](./05_GNS3%20Einstiegsuebung/)**: <br>Diese Anleitung ist gut geeignet um die ersten Schritte mit GNS3 zu lernen. 
 - **[Mein PC pingt ins Labor](./06_GNS3%20Ping%20ins%20Labor%20Uebung/)**:<br> Durch die Verwendung eines Layer2 VPNs in das Labor, kann der persönliche Laptop genauso ins GNS3 Labor eingebunden werden, wie wenn bei einem physischen Labor der eigene Laptop in einen Switch eingesteckt wird. Diese Übung zeigt in einem einfachen Setup wie das eingesetzt werden kann. 
 - **[Basis-Labor](./07_GNS3%20Basis-Labor/)**:<br> Nützliches Basis-Labor mit OpenWRT für verschiedene möglichen Anwendungsfälle

---

# Nützliche Tools

## Terminal Fenster für Windows: Solar Putty
Vorteil dieser Applikation: Alle Terminal fenster werden mit Tabs in einem Fenster geöffnet. Das umschalten zwischen Terminal und GNS3 ist damit viel effizienter. 
 - https://www.solarwinds.com/free-tools/solar-putty


# Weitere Ressourcen

Weitere Ressourcen gibt es auf [LernGNS3](https://github.com/mc-b/lerngns3)
